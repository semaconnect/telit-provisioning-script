#!/usr/bin/env python

'''
*************************************************************************
 *
 * Semaconnect Systems India Pvt Ltd
 * __________________
 * Authors: Ankit Bhatnagar
 * Created: August 2020
 *
 * CSV Writer Class
 *
 * Licence: PROPRIETARY
*************************************************************************
'''

import os
import csv
from helpers.BaseLogger import BaseLogger

class CSVWriter():
    # CSV Writer Class
    
    def __init__(self, file = None, header = None):
        if not file or not header:
            raise TypeError('Required arguments missing !')
        self.__file = file
        self.__csv_header = header
        BaseLogger.info('init. Filename : ' + self.__file)

    def commit(self, data = None):
        # Commit CSV Data To File
        if not data:
            raise TypeError('Required arguments missing')
        # Check If File Exists
        # Set File Write Mode Accordingly
        if bool(os.path.isfile('./' + self.__file)):
            self.__write_mode = 'a'
        else:
            self.__write_mode = 'w'
        with open(self.__file, self.__write_mode, newline = '') as file:
            writer = csv.writer(file)
            if self.__write_mode == 'w':
                # Add CSV Header
                writer.writerow(self.__csv_header)
            for row in data:
                writer.writerow(list(row.values()))
            file.close()
            BaseLogger.info('csv data written to file ' + self.__file + ' writemode : ' + self.__write_mode)