#!/usr/bin/python

'''
*************************************************************************
 *
 * Semaconnect Systems India Pvt Ltd
 * __________________
 * Authors: Ankit Bhatnagar
 * Created: July 2020
 *
 * Script To Provision Telit Modem Modules
 *
 * Licence: PROPRIETARY
*************************************************************************
'''

import logging

# Set Up Base Logger

grey = "\x1b[38;21m"
yellow = "\x1b[33;21m"
red = "\x1b[31;21m"
bold_red = "\x1b[31;1m"
reset = "\x1b[0m"

BaseLogger = logging.getLogger(__name__)

__stream_handler = logging.StreamHandler()
__stream_handler.setFormatter(logging.Formatter('%(asctime)s %(module)-20s %(levelname)-8s %(message)s', '%d-%m-%Y %H:%M:%S'))
__stream_handler.setLevel(logging.DEBUG)

BaseLogger.addHandler(__stream_handler)
BaseLogger.setLevel(logging.INFO)
