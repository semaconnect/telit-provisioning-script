#!/usr/bin/env python

'''
*************************************************************************
 *
 * Semaconnect Systems India Pvt Ltd
 * __________________
 * Authors: Ankit Bhatnagar
 * Created: July 2020
 *
 * OS Specific Class
 *
 * Licence: PROPRIETARY
*************************************************************************
'''

import platform

class OSSpecific():
    # OS Specific Class

    OS_TYPE_LINUX = 1
    OS_TYPE_WINDOWS = 2
    OS_TYPE_UNKNOWN = 3

    @classmethod
    def getOSType(self):
        os = platform.system()
        if os == 'Linux':
            return {'id' : OSSpecific.OS_TYPE_LINUX, 'description' : 'linux'}
        elif os == 'Windows':
            return {'id' : OSSpecific.OS_TYPE_WINDOWS, 'description' : 'windows'}
        return {'id' : OSSpecific.OS_TYPE_UNKNOWN, 'description' : 'unknown'}
