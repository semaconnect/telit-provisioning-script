#!/usr/bin/env python

'''
*************************************************************************
 *
 * Semaconnect Systems India Pvt Ltd
 * __________________
 * Authors: Ankit Bhatnagar
 * Created: August 2020
 *
 * Telit Modem Class
 *
 * Licence: PROPRIETARY
*************************************************************************
'''
import time
from scanf import scanf
from helpers.BaseLogger import BaseLogger

class TelitModem():
    # Telit Modem Class

    TELIT_ESCAPE_SEQUENCE = '+++'
    TELIT_AT_COMMAND_TERMINATION_SEQUENCE = '\r\n'

    TELIT_AT_COMMAND_TIMEOUT_S = 5
    TELIT_AT_COMMAND_RETRY_MAX = 5

    # Telit Modem AT Commands & Responses
    TELIT_AT_COMMAND_CCID = 0
    TELIT_AT_COMMAND_IMEI = 1
    TELIT_AT_COMMAND_GMM = 2
    TELIT_AT_COMMAND_ATE0 = 3

    TELIT_MODEM_AT_COMMANDS = [ \
        'AT#CCID',  # Get CCID
        'AT+CGSN',  # Get IMEI
        'AT+GMM',   # Get Model
        'ATE0'      # Set Echo Off
    ]

    TELIT_SCANF_STRING_GMM = '\r\n%s\r\n\r\nOK\r\n\r\n'
    TELIT_SCANF_STRING_CGSN = '\r\n%s\r\n\r\nOK\r\n\r\n'
    TELIT_SCANF_STRING_CCID = '\r\n#CCID: %s\r\n\r\nOK\r\n\r\n'

    TELIT_RESPONSE_TOKEN_OK = 'OK'
    TELIT_RESPONSE_TOKEN_ERROR = 'ERROR'
    TELIT_RESPONSE_TOKEN_CME_ERROR = '+CME ERROR'

    def __init__(self, serial = None):
        if not serial:
            raise TypeError('required Arguments Missing')
        self.__serial_handle = serial
        BaseLogger.info('init')

    def __execute_at_command(self, command = None):
        # Execute AT Command & Return Response
        if command == None:
            raise TypeError('required Arguments Missing')
        command_counter = 0
        command_response_ok = False
        while command_counter < TelitModem.TELIT_AT_COMMAND_RETRY_MAX:
            #print('counter = ' + str(command_counter))
            command_response = ''
            ts_now = int(time.time())
            try:
                self.__serial_handle.flush()
                self.__serial_handle.clearBuffer()
                self.__serial_handle.write(TelitModem.TELIT_MODEM_AT_COMMANDS[command] + \
                    TelitModem.TELIT_AT_COMMAND_TERMINATION_SEQUENCE)
                while int(time.time()) - ts_now < TelitModem.TELIT_AT_COMMAND_TIMEOUT_S:
                    if self.__get_response_token_type(self.__serial_handle.getBuffer()) == TelitModem.TELIT_RESPONSE_TOKEN_OK:
                        command_response = self.__serial_handle.getBuffer()
                        command_response_ok = True
                        print(command_response)
                        break
                    time.sleep(1)
            except:
                BaseLogger.error('Serial Write Error')
            if bool(command_response_ok):
                break
            command_counter += 1
        if bool(command_response_ok):
            return command_response
        return None

    def __get_response_token_type(self, response = None):
        # Return Response Token Type
        if response == None:
            raise TypeError('required Arguments Missing')
        if TelitModem.TELIT_RESPONSE_TOKEN_OK in response:
            return TelitModem.TELIT_RESPONSE_TOKEN_OK
        elif TelitModem.TELIT_RESPONSE_TOKEN_ERROR in response:
            return TelitModem.TELIT_RESPONSE_TOKEN_ERROR
        elif TelitModem.TELIT_RESPONSE_TOKEN_CME_ERROR in response:
            return TelitModem.TELIT_RESPONSE_TOKEN_CME_ERROR
        else:
            return None
    
    def setATEchoOff(self):
        # Set AT Command Echo Off
        self.__execute_at_command(TelitModem.TELIT_AT_COMMAND_ATE0)
    
    def intialize(self):
        # Initialize Telit Model
        # Run ATE0 Command
        BaseLogger.info('initializing telit modem')
        response = self.__execute_at_command(TelitModem.TELIT_AT_COMMAND_ATE0)
        if not response:
            BaseLogger.error('init failed !')
            self.__initialized = False
            return False
        self.__initialized = True
        return True

    def getModel(self):
        # Get Model Name
        self.__model = '?'
        response = self.__execute_at_command(TelitModem.TELIT_AT_COMMAND_GMM)
        if response:
            self.__model = scanf(TelitModem.TELIT_SCANF_STRING_GMM, response)[0]
        return self.__model

    def getIMEI(self):
        # Get IMEI
        if not bool(self.__initialized):
            return None
        self.__imei = '?'
        response = self.__execute_at_command(TelitModem.TELIT_AT_COMMAND_IMEI)
        if response:
            self.__imei = scanf(TelitModem.TELIT_SCANF_STRING_CGSN, response)[0]
        return self.__imei
    
    def getICCID(self):
        # Get ICCID
        if not bool(self.__initialized):
            return None
        self.__iccid = '?'
        response = self.__execute_at_command(TelitModem.TELIT_AT_COMMAND_CCID)
        if response:
            self.__iccid = scanf(TelitModem.TELIT_SCANF_STRING_CCID, response)[0]
        return self.__iccid

    def getData(self):
        # Get Telit Modem Data

        return { \
            'model' : self.__model, \
            'imei' : self.__imei, \
            'iccid' : self.__iccid \
            }
