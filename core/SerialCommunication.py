#!/usr/bin/env python

'''
*************************************************************************
 *
 * Semaconnect Systems India Pvt Ltd
 * __________________
 * Authors: Ankit Bhatnagar
 * Created: July 2020
 *
 * Serial Communication Class
 *
 * Licence: PROPRIETARY
*************************************************************************
'''

import time
import serial
import threading
from helpers.BaseLogger import BaseLogger

class SerialCommunication(threading.Thread):
    # Serial Communication Class

    SERIAL_BAUDRATE = 115200
    SERIAL_TIMEOUT_S = 2

    def __init__(self, port = None):
        if not port:
            raise TypeError('Required arguments missing !')
        super().__init__()
        self.__serial_handle = None
        self.__serial_present = False
        self.__serial_port = port
        self.__kill = False
        BaseLogger.info('init ' + port)

    def connect(self):
        # Connect Serial Connection
        try:
            self.__serial_handle = serial.Serial(port = self.__serial_port , \
                baudrate = SerialCommunication.SERIAL_BAUDRATE, \
                timeout = SerialCommunication.SERIAL_TIMEOUT_S)
            self.__serial_present = True
            self.__serial_buffer = ''
            BaseLogger.info('serial port open ok')
            return True
        except (OSError, serial.SerialException):
            self.__serial_present = False
            BaseLogger.error('port open error')
            return False
    
    def disconnect(self):
        # Disconnect Serial Connection
        if(self.__serial_present):
            self.__serial_handle.close()
            self.__serial_present = False
            BaseLogger.info('port close')

    def flush(self):
        # Serial Input & Output Pipeline
        self.__serial_handle.flushInput()
        self.__serial_handle.flushOutput()
    
    def getAvailability(self):
        return self.__serial_present
    
    def getBuffer(self):
        return self.__serial_buffer

    def clearBuffer(self):
        self.__serial_buffer = ''
    
    def run(self):
        while True:
            try:
                n = self.__serial_handle.inWaiting() 
                if  n > 0:
                    try:
                        data = self.__serial_handle.read(n).decode('UTF-8')
                        self.__serial_buffer += data
                    except serial.SerialException:
                        pass
            except:
                pass
            if bool(self.__kill):
                break
    
    def kill(self):
        # Set Flag To Kill Thread
        self.__kill = True

    def write(self, data = None):
        # Write Data To Serial
        if not data:
            raise TypeError('Required arguments missing !')
        if len(data) == 0:
            BaseLogger.error('0 len data')
            return
        if not bool(self.__serial_present):
            print('serial fail')
            return
        try:
            self.__serial_handle.write(data.encode(encoding='UTF-8'))
            BaseLogger.info('data written. ' + str(len(data)) + ' bytes')
            BaseLogger.info(data)
        except serial.SerialException:
            self.__serial_present = False
            BaseLogger.error('write error')
            return

