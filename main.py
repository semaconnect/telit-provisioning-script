#!/usr/bin/env python

'''
*************************************************************************
 *
 * Semaconnect Systems India Pvt Ltd
 * __________________
 * Authors: Ankit Bhatnagar
 * Created: August 2020
 *
 * Script To Provision Telit Modem Modules
 *
 * Licence: PROPRIETARY
*************************************************************************
'''

import sys
import time
import datetime
import logging
from core.TelitModem import TelitModem
from core.SerialCommunication import SerialCommunication
from helpers.CSVWriter import CSVWriter
from helpers.OSSpecific import OSSpecific
from helpers.BaseLogger import BaseLogger

if __name__ == '__main__':
    os = OSSpecific.getOSType()
    if os['id'] == 1:
        # Linux
        serial_port_prefix = '/dev/tty'
    elif os['id'] == 2:
        # Windows
        serial_port_prefix = 'COM'
    else:
        # Invalid OS
        BaseLogger.error('unidentified OS !')
        quit()
    BaseLogger.info('OS : ' + os['description'])

    serial_port_id = input("Telit Serial Port : " + serial_port_prefix)
    csv_file = input('CSV Filename : ')
    serial = SerialCommunication(serial_port_prefix + serial_port_id)
    telit_modem = TelitModem(serial)
    
    # Setup Data To Be Pulled In From Telit Modem
    telit_data_table = [ \
        ['MODEL', 'IMEI', 'ICCID'],
        [telit_modem.getModel, telit_modem.getIMEI, telit_modem.getICCID] \
    ]
    csv = CSVWriter(csv_file, telit_data_table[0])

    # Start Serial Communication Thread
    serial.start()

    run_count = 0
    while(True):
        print('***************************')
        print('Run # ' + str(run_count))
        print('***************************')
        response = input('Connect Modem & Press Any Key (Or "q" To End) : ')
        if response.lower() == 's':
            # Change Serial Port
            serial_port_id = input("Telit Serial Port : " + serial_port_prefix)
            serial = SerialCommunication(serial_port_prefix + serial_port_id)
            continue
        
        if response.lower() == 'q':
            # Break Run Cycle Loop
            break
        
        run_count += 1

        # Skip Iteration If Serial Port Connection Failed
        if not bool(serial.connect()):
            continue
        
        # Skip Iteration If Telit Modem Initialization Fail
        if not bool(telit_modem.intialize()):
            continue

        # Get Data From Telit Modem
        telit_modem.setATEchoOff()
        for fn_ptr in telit_data_table[1]:
            fn_ptr()
        
        #modems_data.append(telit_modem.getData())
        # Commit Modem Data To CSV File
        csv.commit([telit_modem.getData()])
        print(str(telit_modem.getData()))
        print('')

        serial.disconnect()

    # Kill Threads And Exit
    serial.kill()